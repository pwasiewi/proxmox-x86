#!/bin/bash

PACKAGES=$(cat <<EOF
corosync-pve.git
openais-pve.git
pve-common.git
libqb.git
libiscsi.git
libnet-http-perl.git
pve-kernel-2.6.32.git
pve-cluster.git
redhat-cluster-pve.git
lvm.git
pve-access-control.git
pve-storage.git
pve-qemu-kvm.git
qemu-server.git
vncterm.git
vzquota.git
vzctl.git
fence-agents-pve.git
resource-agents-pve.git
pve-manager.git
gfs2-utils.git
ksm-control-daemon.git
dab.git
pve-sheepdog.git
EOF
)

function update_repo() {
	# $1 - name
	# $2 - url
	[ ! -e $1 ] && git clone $2 $1 --depth 4
	cd $1
	git fetch --depth 4
	git reset --hard origin/master
	for j in ../patches/*-$1.patch
	do
		[ ! -f $j ] && continue
		echo $j...
		if ! git am $j
		then
			git am --abort
			echo "$1: $j doesn't apply correctly"
			return 1
		fi
	done

	return 0
}

CMD=$1
shift

case $CMD in
	compile)
		[ "$*" != "" ] && PACKAGES=$*

		for i in $PACKAGES
		do
			j=${i//.git}
			[ -e $j.finished ] && continue
			( update_repo $j git://git.proxmox.com/git/$i ) || exit 1
			if [ "$j" == "pve-kernel-2.6.32" ]
			then
				( make -C $j all -j 16 ) || exit 1
			else
				( make -C $j dinstall ) || exit 1
			fi
			touch $j.finished
		done
		;;

	update)
		[ "$*" != "" ] && PACKAGES=$*

		for i in $PACKAGES
		do
			j=${i//.git}
			if ( cd $j; git fetch; git diff origin/master | grep diff > /dev/null )
			then
				echo "$j needs to be compiled"
				rm -f $j.finished
			fi
		done
		;;

	packages)
		echo $PACKAGES
		;;

	prepare)
		apt-get -y install build-essential git-core debhelper autotools-dev \
doxygen check pkg-config libnss3-dev groff quilt dpatch libxml2-dev \
libncurses5-dev libslang2-dev libldap2-dev xsltproc python-pexpect \
python-pycurl libdbus-1-dev openipmi sg3-utils libnet-snmp-perl \
libnet-telnet-perl snmp python-openssl libxml2-utils automake autoconf \
libsqlite3-dev sqlite3 libfuse-dev libglib2.0-dev librrd-dev \
librrds-perl rrdcached lintian libdevel-cycle-perl libjson-perl \
liblinux-inotify2-perl libio-stringy-perl unzip fuse-utils \
libcrypt-openssl-random-perl libcrypt-openssl-rsa-perl \
libauthen-pam-perl libterm-readline-gnu-perl libssl-dev open-iscsi \
libapache2-mod-perl2 libfilesys-df-perl libfile-readbackwards-perl \
libpci-dev texi2html libgnutls-dev libsdl1.2-dev bridge-utils \
libvncserver0 rpm2cpio  apache2-mpm-prefork libintl-perl \
libapache2-request-perl libnet-dns-perl vlan libio-socket-ssl-perl \
libfile-sync-perl ifenslave-2.6 libnet-ldap-perl console-data \
libtool dietlibc-dev liblocale-po-perl libstring-shellquote-perl \
libio-multiplex-perl liblockfile-simple-perl libudev-dev libreadline-gplv2-dev \
libfile-chdir-perl
		;;

	*)
		echo "usage: $0 <command>"
		echo "commands:"
		echo -e "\tcompile <packages>"
		echo -e "\tupdate <packages>"
		echo -e "\tprepare"
		exit 1
		;;
esac

